# Guidelines for contibutors
  * Thank you for participating
  * Fork the repo
  * Write an issue about the problem you want to adress
  * Make a feature branch based on the `dev` branch
  * When you are ready submit a merge request to my `dev` branch

